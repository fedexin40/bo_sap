#!/usr/bin/python


from ansible.module_utils.basic import AnsibleModule


class TimeOutException(Exception):
   pass
   
   

class bo_object(object):

    def __init__(self, ansible_module, installation_path, action):
        self.ansible_module = ansible_module
        self.installation_path = installation_path
        self.action = action

    def __is_still_running(self, command):
        executable_path = ''
        try:
            executable_path = self._execute(command).split()[0]
        except IndexError:
            pass
        return executable_path

    def is_still_running(self):
        # In order to get the installation_path I am going 
        # to execute below bash command
        return self.__is_still_running(self.is_still_running_command)

    def start(self):
        pass
    
    def stop(self):
        pass

    def _execute(self, command, with_user_var=False):
        """
        Execute commands
        """
        # TODO: Fix below forcing bash command
        if with_user_var:
            command = ''.join(['/bin/bash -l -c ', command])
        status, message, _ = self.ansible_module.run_command(
            args=command, use_unsafe_shell=True
        )
        if status != 0 or 'Warning' in message:
            message = 'Command {command} fails with message: {message}'.format(
                command=command, message=message)
            self.ansible_module.fail_json(msg=message)
        else:
            return message
            
    def perform_action(self):
        # Execute action, it could be start or stop
        if 'start' == self.action:
            self.start()
        else:
            self.stop()

    @property
    def status(self):
       # Maybe the is_still_running function is not appropiate
       # but I am taking advanantage of the already existing funcionts
       result = self.is_still_running()
       if result:
           return 'Running'
       else:
           return 'Stopped'

