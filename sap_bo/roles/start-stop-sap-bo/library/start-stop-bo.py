#!/usr/bin/python

import os
import re
import signal
import time

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.start_stop import TimeOutException
from ansible.module_utils.start_stop import bo_object


class bo(bo_object):

    def __init__(
            self, module, action, installation_path, installation_owner,
            with_command, servers='', username='', password=''):
        super(bo, self).__init__(module, installation_path, action)
        self.is_still_running_command = "ps -eo cmd | grep '[s]ap_bobj' | grep  _boe_  | tail -1"
        self.with_command = with_command
        self.servers = servers
        self.username = username
        self.password = password
        self.installation_owner = installation_owner

    def _stop_with_sia(self):
        # Function uses to stop the complete SIA
        command = os.path.join(self.installation_path, 'stopservers')
        self._execute(command)
        # On above line I stopped the SIA
        # So of after stopping SIA there is still alive processes
        # We need to kill them, refer to SAP Note 1200480 
        command = (
            "kill -9 `ps -fu {username}  | grep -i Xvfb | grep -v grep | awk '{{ print $2 }}'`"
        ).format(username=self.installation_owner)
        self._execute(command)
        command = (
            "ps -fea | grep '[s]ap_bobj' | grep -vi tomcat | awk '{ print $2}'"
        )
        pids = self._execute(command)
        pids = pids.replace('\n', ' ')
        command = "kill -p {pids}".format(pids=pids)
        # Remove process that are still in severpids folder
        serverpids_path = os.path.join(self.installation_path, 'serverpids/*')
        bash_command = ''.join(['rm -f ', serverpids_path])
        self._execute(bash_command)

    def _stop_with_ccm(self, installation_path):
        # Function uses to stop BO servers using the ccm.sh command
        pass

    def stop(self):
        if self.status == 'Stopped':
            return
        # Stop servers depends on user wants to stop all SIA
        # or only some servers using ccm.sh script
        if self.with_command == 'ccm':
            self._stop_with_ccm()
        else:
            self._stop_with_sia()

    def _start_with_sia(self):
        # Function uses to start the complete SIA
        command = os.path.join(self.installation_path, 'startservers')
        self._execute(command, with_user_var=True)

    def _start_with_ccm(self, installation_path):
        # Function uses to start BO servers using the ccm.sh command
        pass
    
    def start(self):
        # Start servers depends on user wants to stop all SIA
        # or only some servers using ccm.sh script
        if self.status == 'Running':
            return
        if self.with_command == 'ccm.sh':
            self._start_with_ccm()
        else:
            self._start_with_sia()
# End of stop functions


def _alarm_handler(signum, frame):
    raise TimeOutException()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            action = dict(required=True, choices=['start', 'stop',]),
            timeout   = dict(required=False, type='int', default=10, help='Time is in minutes'),
            installation_path = dict(required=True),
            installation_owner = dict(required=False),
            with_command = dict(default='SIA', choices=['SIA', 'ccm',]),
            servers   = dict(),
            username = dict(),
            password = dict(),
        )
    )
    action = module.params['action']
    installation_path = module.params['installation_path']
    with_command = module.params['with_command']
    servers = module.params['servers']
    username = module.params['username']
    password = module.params['password']
    installation_owner = module.params['installation_owner']
    bo_instance = bo(
        module, action, installation_path, installation_owner,
        with_command, servers, username, password)
    # Setup timeout
    signal.signal(signal.SIGALRM, _alarm_handler)
    signal.alarm(module.params['timeout'] * 60)
    # Execute instruction but with timeout
    try:
        bo_instance.perform_action()
    except TimeOutException:
        module.fail_json(
            msg="It was not possible to stop the servers "
            " Please check the logs for more information"
        )
    # Disable signal alarm
    signal.alarm(0)
    module.exit_json()


if __name__ == '__main__':
      main()
