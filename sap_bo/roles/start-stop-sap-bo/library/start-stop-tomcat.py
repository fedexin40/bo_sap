#!/usr/bin/python

import os
import re
import signal
import time

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.start_stop import TimeOutException
from ansible.module_utils.start_stop import bo_object


class tomcat(bo_object):
    
    def __init__(self, module, installation_path, action):
        super(tomcat, self).__init__(module, installation_path, action)
        self.is_still_running_command = (
            "ps -eo cmd | grep {installation_path} | grep '[t]omcat' | tail -1").format(
                installation_path=self.installation_path)

    def stop(self):
        if self.status == 'Stopped':
            return
        command = os.path.join(self.installation_path, 'tomcatshutdown.sh')
        self._execute(command)
        while self.is_still_running():
            pass
        return
        
    def start(self):
        if self.status == 'Running':
            return
        command = os.path.join(self.installation_path, 'tomcatstartup.sh')
        self._execute(command)


def _alarm_handler(signum, frame):
    raise TimeOutException()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            action = dict(required=True, choices=['start', 'stop',]),
            installation_path = dict(required=True),
            timeout   = dict(required=False, type='int', default=10, help='Time is in minutes'),
        )
    )
    action = module.params['action']
    installation_path = module.params['installation_path']
    tomcat_instance = tomcat(module, installation_path, action)
    # Setup timeout
    signal.signal(signal.SIGALRM, _alarm_handler)
    signal.alarm(module.params['timeout'] * 60)
    # Execute instruction but with timeout
    try:
        tomcat_instance.perform_action()
    except TimeOutException:
        module.fail_json(
            msg="It was not possible to stop the servers "
            " Please check the logs for more information"
        )
    # Disable signal alarm
    signal.alarm(0)
    module.exit_json()

if __name__ == '__main__':
      main()
