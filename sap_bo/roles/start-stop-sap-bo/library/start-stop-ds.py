#!/usr/bin/python

import os
import re
import signal
import time

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.start_stop import TimeOutException
from ansible.module_utils.start_stop import bo_object
from os.path import dirname as up


class ds(bo_object):

    def __init__(self, module, action, installation_path): 
        super(ds, self).__init__(module, installation_path, action)
        self.is_still_running_command = (
            "ps -eo cmd | grep {ds_install_path} | grep -vi tomcat".format(
                 ds_install_path=self.installation_path_ds))

    @property
    def installation_path_ds(self):
        # This is an error
        # The installation_path is wrong it needs to be fixed
        # but a lot of things should change
        # TODO: Fix self.installation_path
        sap_installation = up(os.path.join(self.installation_path))
        return os.path.join(self.installation_path, "../dataservices/bin")
    
    def stop(self):
        if self.status == 'Stopped':
            return
        command = 'cd {installation_path_ds} && . ./al_env.sh && ./svrcfg -TSTOP'.format(
            installation_path_ds=self.installation_path_ds)
        self._execute(command)
    
    def start(self):
        if self.status == 'Running':
            return
        command = 'cd {installation_path_ds} && . ./al_env.sh && ./svrcfg -TSTART'.format(
            installation_path_ds=self.installation_path_ds)
        self._execute(command)


def _alarm_handler(signum, frame):
    raise TimeOutException()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            action = dict(required=True, choices=['start', 'stop',]),
            timeout   = dict(required=False, type='int', default=10, help='Time is in minutes'),
            installation_path = dict(required=True),
        )
    )
    action = module.params['action']
    installation_path = module.params['installation_path']
    ds_instance = ds(module, action, installation_path)
    # Setup timeout
    signal.signal(signal.SIGALRM, _alarm_handler)
    signal.alarm(module.params['timeout'] * 60)
    # Execute instruction but with timeout
    try:
        ds_instance.perform_action()
    except TimeOutException:
        module.fail_json(
            msg="It was not possible to stop the servers "
            " Please check the logs for more information"
        )
    # Disable signal alarm
    signal.alarm(0)
    module.exit_json()


if __name__ == '__main__':
      main()
