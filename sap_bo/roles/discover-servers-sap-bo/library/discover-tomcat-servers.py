#!/usr/bin/python

import os
import re
import signal
import time
import subprocess

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.discover_servers import TimeOutException
from ansible.module_utils.discover_servers import bo_object


class tomcat(bo_object):

    def __init__(self, module, hostname):
        super(tomcat, self).__init__(module)
        self.discover_command = "ps -eo cmd | grep '[t]omcat' | grep {install_path} | tail -1".format(
            install_path=self.install_path)
        self.hostname = hostname

    @property
    def tomcat_pid(self):
        # Function to get the tomcat PID
        command = "ps -fea | grep '[t]omcat' | grep {install_path} | awk '{{ print $2 }}'".format(
	    install_path=self.install_path)
        res = subprocess.check_output(
            command, shell=True, env={}).decode('utf-8')
        return res.strip()

    @property
    def tomcat_url(self):
        # This function will return a string with
        # the base tomcat URL
        # For example:
        #   https://<hostname>:8443 or
        #   http://<hostname>:8080
        # TODO: Below command is to get the URL tomcat is running
        # but it using a bash command, is it really the best?
        # Take care that it is not possible to install any
        # python package in the remote system. Customer policies
        command = "netstat -tulpn 2>/dev/null | grep {tomcat_pid} | sed -n 2p | awk '{{ print $4 }}'".format(
            tomcat_pid=self.tomcat_pid)
        port = self._execute_bash(command)
        # Strip res to get only port
        if not port:
            return False
        port = re.findall('[0-9]+$', port)[0]
        # Try to connect using https
        url = "https://{hostname}:{port}".format(hostname=self.hostname, port=port)
        command = "curl --noproxy '*' -Iks {url} | head -n 1|cut -d$' ' -f2".format(url=url)
        http_code = self._execute_bash(command).strip()
        if http_code == '200':
            return url
        # Try to connect using http
        url = "http://{hostname}:{port}".format(hostname=self.hostname, port=port)
        command = "curl --noproxy '*' -Iks {url} | head -n 1|cut -d$' ' -f2".format(url=url)
        http_code = self._execute_bash(command).strip()
        if http_code == '200':
            return url

    @property
    def tomcat_root_path(self):
        return os.path.join(self.install_path, 'tomcat')

    def web_apps(self):
        web_apps_path = os.path.join(self.tomcat_root_path, 'webapps')
        for web_app in os.listdir(web_apps_path):
            web_app = os.path.join(web_apps_path, web_app)
            if os.path.isdir(web_app):
                yield web_app

    def web_apps_properties(self, web_app):
        config = os.path.join(web_app, 'WEB-INF/config/default')
        if not os.path.exists(config):
            return
        for file in os.listdir(config):
            if file.endswith('.properties'):
                yield os.path.join(config, file)

    def read_boservices_running(self):
        # This function will return the complete URLs for the
        # tomca webapps
        # It will lookup for each property inside the
        # <tomcat_installation>/webapps folder
        # But only return the url if it is running well, with http code 200
        urls = []
        urls_by_app = []
        for web_app in self.web_apps():
            for property in self.web_apps_properties(web_app):
                file = open(property, 'r')
                content = file.read()
                file.close()
                match = re.findall('app.url.name=.*', content)
                if not match:
                    continue
                match = match[0].replace('app.url.name=', '')
                web_app = web_app.split('/')[-1]
                url = ''.join([web_app, match])
                url = '/'.join([self.tomcat_url, url])
                command = "curl --noproxy '*' -Iks {url} | head -n 1|cut -d$' ' -f2".format(url=url)
                http_code = self._execute_bash(command).strip()
                if http_code == '200':
                    urls.append(url)
        return urls


def _alarm_handler(signum, frame):
    raise TimeOutException()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            timeout   = dict(required=False, type='int', default=10, help='Time is in minutes'),
            hostname  = dict(required=True, type='str'),
        )
    )
    # Setup timeout
    hostname = module.params['hostname']
    tomcat_instance = tomcat(module, hostname)
    signal.signal(signal.SIGALRM, _alarm_handler)
    signal.alarm(module.params['timeout'] * 60)
    # Execute instruction but with timeout
    try:
        webapps = tomcat_instance.read_boservices_running()
    except TimeOutException:
        module.fail_json(
            msg="It was not possible to stop the servers "
            " Please check the logs for more information"
    )
    # Disable signal alarm
    signal.alarm(0)
    # Return result
    result = {'webapps': webapps}
    module.exit_json(**result)

if __name__ == '__main__':
      main()
