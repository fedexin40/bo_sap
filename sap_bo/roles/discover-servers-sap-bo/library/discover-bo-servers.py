#!/usr/bin/python

import os
import re
import signal
import time
import subprocess

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.discover_servers import TimeOutException
from ansible.module_utils.discover_servers import bo_object
from functools import wraps


class bo(bo_object):

    def __init__(self, module, username, password):
        super(bo, self).__init__(module)
        self.discover_command = "ps -eo cmd | grep '[s]ap_bobj' | grep  _boe_ |tail -1"
        self.username = username
        self.password = password
    
    def memoize(func):
        cache = func.cache = {}
        @wraps(func)
        def memoized_func(*args, **kwargs):
            key = str(args) + str(kwargs)
            if key not in cache:
                cache[key] = func(*args, **kwargs)
            return cache[key]
        return memoized_func

    @memoize
    def servers(self):
        # This function gets the output of the command
        # ccm.sh whitout modification, it is a string
        # It will return a list of dictionaries with the following
        #   server_name: {{ Server Name }}
        #   state: {{ State }}
        #   enabled: {{ Enabled }}
        #   description: {{ Description }}
        #   pid: {{ pid }}a
        # Below paratmers are only for display the
        # BO processes running in the system
        ccm_parameters = ('-display -password {password} '
            '-username {username}'.format(
                password=self.password, username=self.username))
        ccm = ' '.join([self.ccm_command, ccm_parameters])
        servers = self._execute(ccm)
        servers = servers.split('\n\n')
        # Below I am assuming that the ccm.sh output is like below
        #   Creating session manager...
        #   Logging onto CMS...
        #   Creating infostore...
        #   Sending query to get all server objects on the local machine...
        #   Checking server status...
        # That's why I am removing the first element in the list
        servers.pop(0)
        # Remove empty elements
        servers = filter(None, servers)
        server_list = []
        for server in servers:
            server = [x.strip() for x in server.split('\n')]
            server_name = [x for x in server if 'Server Name' in x]
            state = [x for x in server if 'State' in x]
            enabled = [x for x in server if 'Enabled' in x]
            server = {
                'server_name': server_name[0].replace(
                                        ':', '').replace('Server Name', '').strip(),
                'state': state[0].replace(':', '').replace('State','').strip(),
                'enabled': enabled[0].replace(':', '').strip(),
            }
            server_list.append(server)
        return server_list

    @property
    def ccm_command(self):
        # Return: @String - It returns ccm.sh absolute path
        return os.path.join(self.install_path, 'ccm.sh')

    @property
    def inventory_path(self):
        # Return: @String - It returns Inventory file absolute path
        return os.path.join(self.install_path, '../InstallData/inventory.txt')

    @property
    def does_it_have_ds(self):
        # Below part is used to know if Data Services is instaled
        command = ' '.join(['cat', self.inventory_path])
        inventory = self._execute(command)
        inventory = re.sub(r'[\W_]+', '', inventory)
        if 'DataServices' in inventory:
            return True
        else:
            return False

    @property
    def does_it_have_cms(self):
        for server in self.servers():
            value = server.values()[2]
            if 'CentralManagementServer' in value:
                return True
        return False

    @property
    def does_it_have_repository(self):
        for server in self.servers():
            value = server.values()[2]
            if 'FileRepository' in value:
                return True
        return False


def _alarm_handler(signum, frame):
    raise TimeOutException()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            username  = dict(required=False, type='str'),
            password  = dict(required=False, type='str'),
            timeout   = dict(required=False, type='int',default=10, help='Time is in minutes'),
        )
    )
    username = module.params['username']
    password = module.params['password']
    bo_instance = bo(module, username, password)
    # Setup timeout
    signal.signal(signal.SIGALRM, _alarm_handler)
    signal.alarm(module.params['timeout'] * 60)
    # Execute instructions but with timeout
    try:
        servers = bo_instance.servers()
        installation_path = bo_instance.install_path
        cms = bo_instance.does_it_have_cms
        filerepository = bo_instance.does_it_have_repository
        ds = bo_instance.does_it_have_ds
    except TimeOutException:
        module.fail_json(
            msg="It was not possible to stop the servers "
            " Please check the logs for more information"
    )
    # Disable signal alarm
    signal.alarm(0)
    result = {
        'installation_path': installation_path,
        'servers': servers,
        'cms': cms,
        'filerepository': filerepository,
        'ds': ds,
    }
    module.exit_json(**result)

if __name__ == '__main__':
      main()
