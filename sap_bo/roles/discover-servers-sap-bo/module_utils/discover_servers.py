#!/usr/bin/python

import os
import re
import subprocess


class TimeOutException(Exception):
   pass


class bo_object(object):

    def __init__(self, ansible_module):
        self.ansible_module = ansible_module
        self.discover_command = ""

    def _install_path(self, command):
        executable_path = ''
        try:
            executable_path = self._execute(command).split()[0]
        except IndexError:
            pass
        return executable_path

    @property
    def install_path(self):
        # In order to get the installation_path I am going
        # to execute below bash command
        # Return: @String - It returns sap_bobj absolute path
        path_install = self._install_path(self.discover_command)
        # TODO: I did not find a way to remove all string except ^.*sap_bobj
        # So I am going to add again sap_bobj after removing it
        path_install = re.sub('(sap_bobj.*)', '', path_install)
        return os.path.join(path_install, 'sap_bobj')

    def _execute(self, command):
        """
        Execute commands
        """
        status, message, _ = self.ansible_module.run_command(
            args=command, use_unsafe_shell=True
        )
        if status != 0 or 'Warning' in message:
            message = 'Command {command} fails with message: {message}'.format(
                command=command, message=message)
            self.ansible_module.fail_json(msg=message)
        else:
            return message

    def _execute_bash(self, command):
        result = subprocess.check_output(
            command, shell=True, env={}).decode('utf-8')
        return result
